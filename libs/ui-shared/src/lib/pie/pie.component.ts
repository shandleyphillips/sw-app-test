import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { FriendsEntity } from '@sw-app-test/friends';
import { Observable } from 'rxjs';
import * as d3 from 'd3';

@Component({
  selector: 'sw-app-test-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.scss'],
})
export class PieComponent implements OnChanges {
  hostElement: any;
  constructor(private elRef: ElementRef) {
    this.hostElement = this.elRef.nativeElement;
  }

  @Input() friends!: Observable<FriendsEntity[]>;

  private svg: any;
  private margin = 50;
  private width = 750;
  private height = 600;
  // The radius of the pie chart is half the smallest side
  private radius = Math.min(this.width, this.height) / 2 - this.margin;
  private colors: any;
  data!: FriendsEntity[];

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.friends) {
      changes.friends.currentValue.subscribe((x: FriendsEntity[]) => {
        this.data = x;
        this.createChart(this.data);
      });
    }
  }

  private createChart(data: FriendsEntity[]) {
    this.removeExistingChartFromParent();
    this.createSvg();
    this.drawChart(data);
    this.createColors;
  }

  private createSvg(): void {
    this.svg = d3
      .select('figure#pie')
      .append('svg')
      .attr('width', this.width)
      .attr('height', this.height)
      .append('g')
      .attr(
        'transform',
        'translate(' + this.width / 2 + ',' + this.height / 2 + ')'
      );
  }

  private createColors(): void {
    this.colors = d3
      .scaleOrdinal()
      .domain(this.data.map((d: FriendsEntity) => d.age.toString()))
      .range(['ffffff']);
  }

  private drawChart(data: FriendsEntity[]): void {
    // Compute the position of each group on the pie:
    const pie = d3.pie<any>().value((d: FriendsEntity) => Number(d.age));

    // Build the pie chart
    this.svg
      .selectAll('pieces')
      .data(pie(data))
      .enter()
      .append('path')
      .attr('d', d3.arc().innerRadius(0).outerRadius(this.radius))
      .attr('fill', '#ededed')
      .attr('stroke', '#121926')
      .style('stroke-width', '1px');

    // Add labels
    const labelLocation = d3.arc().innerRadius(100).outerRadius(this.radius);

    this.svg
      .selectAll('pieces')
      .data(pie(data))
      .enter()
      .append('text')
      .text((d: FriendsEntity) => d.name)
      .attr(
        'transform',
        (d: any) => 'translate(' + labelLocation.centroid(d) + ')'
      )
      .style('text-anchor', 'middle')
      .style('font-size', 15);
  }

  public updateChart(data: FriendsEntity[]) {
    if (!this.svg) {
      this.createChart(data);
      return;
    }
  }

  private removeExistingChartFromParent() {
    // !!!!Caution!!!
    // Make sure not to do;
    //     d3.select('svg').remove();
    // That will clear all other SVG elements in the DOM
    d3.select(this.hostElement).select('svg').remove();
  }
}

import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { FriendsEntity } from '@sw-app-test/friends';
import { Observable } from 'rxjs';
import * as d3 from 'd3';

@Component({
  selector: 'sw-app-test-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnChanges {
  hostElement: any;
  constructor(private elRef: ElementRef) {
    this.hostElement = this.elRef.nativeElement;
  }

  @Input() friends!: Observable<FriendsEntity[]>;

  private svg: any;
  private margin = 50;
  private width = 750 - this.margin * 2;
  private height = 400 - this.margin * 2;
  data!: FriendsEntity[];

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.friends) {
      changes.friends.currentValue.subscribe((x: FriendsEntity[]) => {
        this.data = x;
        this.createChart(this.data);
      });
    }
  }

  private createChart(data: FriendsEntity[]) {
    this.removeExistingChartFromParent();
    this.createSvg();
    this.drawBars(data);
  }

  private createSvg(): void {
    this.svg = d3
      .select('figure#bar')
      .append('svg')
      .attr('width', this.width + this.margin * 2)
      .attr('height', this.height + this.margin * 2)
      .append('g')
      .attr('transform', 'translate(' + this.margin + ',' + this.margin + ')');
  }

  private drawBars(data: FriendsEntity[]): void {
    // Add X axis
    const x = d3
      .scaleBand()
      .range([0, this.width])
      .domain(data.map((d: FriendsEntity) => d.name))
      .padding(0.2);
    this.svg
      .append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(d3.axisBottom(x))
      .selectAll('text')
      .attr('transform', 'translate(-10,0)rotate(-45)')
      .style('text-anchor', 'end');

    // Add Y axis
    const y = d3.scaleLinear().domain([0, 2000]).range([this.height, 0]);

    this.svg.append('g').call(d3.axisLeft(y));

    // Create and fill the bars
    this.svg
      .selectAll('bars')
      .data(data)
      .enter()
      .append('rect')
      .attr('x', (d: FriendsEntity) => x(d.name))
      .attr('y', (d: FriendsEntity) => y(d.weight))
      .attr('width', x.bandwidth())
      .attr('height', (d: FriendsEntity) => this.height - y(d.weight))
      .attr('fill', '#e91e63');

    this.svg
      .selectAll('rect')
      .transition()
      .duration(800)
      .attr('y', (d: FriendsEntity) => y(d.weight))
      .attr('height', (d: FriendsEntity) => this.height - y(d.weight))
      .delay('', (d: FriendsEntity, i: any) => {
        console.log(i);
        i * 100;
      });
  }

  public updateChart(data: FriendsEntity[]) {
    if (!this.svg) {
      this.createChart(data);
      return;
    }
  }

  private removeExistingChartFromParent() {
    // !!!!Caution!!!
    // Make sure not to do;
    //     d3.select('svg').remove();
    // That will clear all other SVG elements in the DOM
    d3.select(this.hostElement).select('svg').remove();
  }
}

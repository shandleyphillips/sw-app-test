import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ListComponent } from './list.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';
import {
  addFriend,
  FriendsEntity,
  initialState,
  reducer,
  State,
} from '@sw-app-test/friends';
import { cold, getTestScheduler, hot } from 'jasmine-marbles';
import { Store, StoreModule } from '@ngrx/store';
import * as fromRoot from '@sw-app-test/friends';
import { of } from 'rxjs';
import { DebugElement } from '@angular/core';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [ListComponent],
      imports: [
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatTableModule,
        MatDividerModule,
        StoreModule.forRoot(fromRoot.reducer),
      ],
      providers: [
        {
          provide: Store,
          useValue: {
            dispatch: jest.fn(),
            pipe: jest.fn(),
          },
        },
      ],
    });
    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('friends', () => {
    //   it('should show a list of friends', (done) => {
    //     const myFriends = [
    //       { id: '1', name: 'Brian Jest', age: 10, weight: 70, numFriends: 20 },
    //       { id: '2', name: 'Suzzy Jest', age: 15, weight: 90, numFriends: 30 },
    //       { id: '3', name: 'Jerry Jest', age: 20, weight: 110, numFriends: 40 },
    //     ];
    //     component.friends = of(myFriends);
    //     component.friends.pipe = jest.fn(() => cold('-a|', { a: myFriends }));

    //     component.friends.pipe().subscribe((componentFriends) => {
    //       expect(componentFriends).toEqual(myFriends);

    //       fixture.detectChanges();
    //       const friendTableCells = fixture.debugElement.queryAll(
    //         By.css('mat-cell')
    //       );
    //       const friendDataCell = friendTableCells.map(
    //         (debugElement: DebugElement) => debugElement.nativeElement.innerHTML
    //       );

    //       expect(friendDataCell).toEqual(`${componentFriends[0].name} `);
    //     });

    //     getTestScheduler().flush();
    //   });

    it('should emit and id', () => {
      const friend: FriendsEntity = {
        id: '1',
        name: 'Brian Jest',
        age: 10,
        weight: 70,
        numFriends: 20,
      };

      const action = addFriend({ friend: friend });
      const result: State = reducer(initialState, action);

      expect(result.ids.length).toBe(1);
    });
    it('should be an observable of an array of friendsEntity objects', (done) => {
      const myFriends = [
        { id: '1', name: 'Brian Jest', age: 10, weight: 70, numFriends: 20 },
        { id: '2', name: 'Suzzy Jest', age: 15, weight: 90, numFriends: 30 },
        { id: '3', name: 'Jerry Jest', age: 20, weight: 110, numFriends: 40 },
      ];
      component.friends = of(myFriends);
      component.friends.pipe = jest.fn(() => cold('-a|', { a: myFriends }));

      component.friends.pipe().subscribe((componentFriends) => {
        expect(componentFriends).toEqual(myFriends);
        done();
      });

      getTestScheduler().flush();
    });
  });
});

import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Observable } from 'rxjs';
import { FriendsEntity } from '@sw-app-test/friends';

@Component({
  selector: 'sw-app-test-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnChanges {
  @Input() friends!: Observable<FriendsEntity[]>;

  displayedColumns!: string[];
  columnsToDisplay!: string[];

  data!: FriendsEntity[];
  tableData!: FriendsEntity[];

  // deletedFriend listening for id of deleted friend
  @Output() deletedFriend = new EventEmitter<string>();
  // deleteFriend emits friend.id passed from button up to friend.list through output
  deleteFriend(id: any) {
    this.deletedFriend.emit(id);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.friends) {
      changes.friends.currentValue.subscribe((x: FriendsEntity[]) => {
        this.data = x;
        this.displayedColumns = ['name', 'age', 'weight', 'numFriends', 'id'];
      });
    }
  }
}

import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { FriendsEntity } from '@sw-app-test/friends';
import { Observable } from 'rxjs';
import * as d3 from 'd3';

@Component({
  selector: 'sw-app-test-scatter',
  templateUrl: './scatter.component.html',
  styleUrls: ['./scatter.component.scss'],
})
export class ScatterComponent implements OnChanges {
  hostElement: any;
  constructor(private elRef: ElementRef) {
    this.hostElement = this.elRef.nativeElement;
  }

  @Input() friends!: Observable<FriendsEntity[]>;

  private svg: any;
  private margin = 50;
  private width = 750 - this.margin * 2;
  private height = 400 - this.margin * 2;
  data!: FriendsEntity[];

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.friends) {
      changes.friends.currentValue.subscribe((x: FriendsEntity[]) => {
        this.data = x;
        this.createChart(this.data);
      });
    }
  }

  private createChart(data: FriendsEntity[]) {
    this.removeExistingChartFromParent();
    this.createSvg();
    this.drawPlot(data);
  }

  private createSvg(): void {
    this.svg = d3
      .select('figure#scatter')
      .append('svg')
      .attr('width', this.width + this.margin * 2)
      .attr('height', this.height + this.margin * 2)
      .append('g')
      .attr('transform', 'translate(' + this.margin + ',' + this.margin + ')');
  }

  private drawPlot(data: FriendsEntity[]): void {
    // Add X axis
    const x = d3.scaleLinear().domain([0, 200]).range([0, this.width]);
    this.svg
      .append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(d3.axisBottom(x).tickFormat(d3.format('d')));

    // Add Y axis
    const y = d3.scaleLinear().domain([0, 300]).range([this.height, 0]);
    this.svg.append('g').call(d3.axisLeft(y));

    // Add dots
    const dots = this.svg.append('g');
    dots
      .selectAll('dot')
      .data(data)
      .enter()
      .append('circle')
      .attr('cx', (d: FriendsEntity) => x(d.age))
      .attr('cy', (d: FriendsEntity) => y(d.numFriends))
      .attr('r', 7)
      .style('opacity', 0.5)
      .style('fill', '#69b3a2');

    // Add labels
    dots
      .selectAll('text')
      .data(data)
      .enter()
      .append('text')
      .text((d: FriendsEntity) => d.name)
      .attr('x', (d: FriendsEntity) => x(d.age))
      .attr('y', (d: FriendsEntity) => y(d.numFriends));
  }

  public updateChart(data: FriendsEntity[]) {
    if (!this.svg) {
      this.createChart(data);
      return;
    }
  }

  private removeExistingChartFromParent() {
    // !!!!Caution!!!
    // Make sure not to do;
    //     d3.select('svg').remove();
    // That will clear all other SVG elements in the DOM
    d3.select(this.hostElement).select('svg').remove();
  }
}

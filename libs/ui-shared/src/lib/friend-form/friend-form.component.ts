import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { FriendsEntity } from '@sw-app-test/friends';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'sw-app-test-friend-form',
  templateUrl: './friend-form.component.html',
  styleUrls: ['./friend-form.component.scss'],
})
export class FriendFormComponent {
  friendFormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(2)]),
    age: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
    ]),
    weight: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
    ]),
    numFriends: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
    ]),
  });
  @ViewChild(FormGroupDirective) formRef!: FormGroupDirective;
  @Output() addedFriend = new EventEmitter<FriendsEntity>();
  newFriendObject!: FriendsEntity;

  onSubmit() {
    this.newFriendObject = this.friendFormGroup.value;
    this.newFriendObject = { ...this.newFriendObject, id: uuidv4() };
    this.addedFriend.emit(this.newFriendObject);
  }

  reset(): void {
    // from @ViewChild
    this.formRef.resetForm();
  }
}


        import { NgModule } from '@angular/core';
        import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromFriends from './+state/friends/friends.reducer';
import { FriendsEffects } from './+state/friends/friends.effects';
        
        @NgModule({
          imports: [
            CommonModule,
            StoreModule.forFeature(fromFriends.FRIENDS_FEATURE_KEY, fromFriends.reducer),
            EffectsModule.forFeature([FriendsEffects])
          ]
        })
        export class FriendsModule { }
        
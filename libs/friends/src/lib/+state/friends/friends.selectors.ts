import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FRIENDS_FEATURE_KEY, State, friendsAdapter } from './friends.reducer';

// Lookup the 'Friends' feature state managed by NgRx
export const getFriendsState = createFeatureSelector<State>(FRIENDS_FEATURE_KEY);

const { selectAll, selectEntities } = friendsAdapter.getSelectors();

export const getFriendsLoaded = createSelector(
  getFriendsState,
  (state: State) => state.loaded
);

export const getFriendsError = createSelector(
  getFriendsState,
  (state: State) => state.error
);

export const getAllFriends = createSelector(
  getFriendsState,
  (state: State) => selectAll(state)
);

export const getFriendsEntities = createSelector(
  getFriendsState,
  (state: State) => selectEntities(state)
);

export const getSelectedId = createSelector(
  getFriendsState,
  (state: State) => state.selectedId
);

export const getSelected = createSelector(
  getFriendsEntities,
  getSelectedId,
  (entities, selectedId) => (selectedId ? entities[selectedId] : undefined)
);

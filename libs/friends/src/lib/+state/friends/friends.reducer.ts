import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';

import * as FriendsActions from './friends.actions';
import { FriendsEntity } from './friends.models';

export const FRIENDS_FEATURE_KEY = 'friends';

export interface State extends EntityState<FriendsEntity> {
  selectedId?: string | number; // which Friends record has been selected
  loaded: boolean; // has the Friends list been loaded
  error?: string | null; // last known error (if any)
}

export interface FriendsPartialState {
  readonly [FRIENDS_FEATURE_KEY]: State;
}

export const friendsAdapter: EntityAdapter<FriendsEntity> =
  createEntityAdapter<FriendsEntity>();

export const initialState: State = friendsAdapter.getInitialState({
  // set initial required properties
  loaded: false,
});

const friendsReducer = createReducer(
  initialState,
  on(FriendsActions.init, (state) => ({
    ...state,
    loaded: false,
    error: null,
  })),
  // Add friend action so we can remove a friend.
  // addOne is built in method from entityAdapter
  on(FriendsActions.addFriend, (state, { friend }) => {
    return friendsAdapter.addOne(friend, state);
  }),
  // Delete friend action so we can remove a friend.
  // removeOne is built in method from entityAdapter
  on(FriendsActions.deleteFriend, (state, { id }) => {
    return friendsAdapter.removeOne(id, state);
  }),
  on(FriendsActions.loadFriendsSuccess, (state, { friends }) =>
    friendsAdapter.setAll(friends, { ...state, loaded: true })
  ),
  on(FriendsActions.loadFriendsFailure, (state, { error }) => ({
    ...state,
    error,
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return friendsReducer(state, action);
}

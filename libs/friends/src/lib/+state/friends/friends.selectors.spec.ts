import { FriendsEntity } from './friends.models';
import {
  friendsAdapter,
  FriendsPartialState,
  initialState,
} from './friends.reducer';
import * as FriendsSelectors from './friends.selectors';

describe('Friends Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getFriendsId = (it: FriendsEntity) => it.id;
  const createFriendsEntity = (
    id: string,
    name = '',
    age = 5,
    weight = 46,
    numFriends = 10
  ): FriendsEntity => ({
    id,
    name: name || `name-${id}`,
    age: age,
    weight: weight,
    numFriends: numFriends,
  });

  let state: FriendsPartialState;

  beforeEach(() => {
    state = {
      friends: friendsAdapter.setAll(
        [
          createFriendsEntity('1', 'Brian Jest', 10, 70, 20),
          createFriendsEntity('2', 'Suzzy Jest', 15, 90, 30),
          createFriendsEntity('3', 'Jerry Jest', 20, 110, 40),
        ],
        {
          ...initialState,
          selectedId: '3',
          error: ERROR_MSG,
          loaded: true,
        }
      ),
    };
  });
  // With loaded Friends set selectedid (not used yet) and validate
  // that the number of results is as expect and the setId is as expected.
  describe('Friends Selectors', () => {
    it('getAllFriends() should return the list of Friends', () => {
      const results = FriendsSelectors.getAllFriends(state);
      const selId = getFriendsId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('2');
    });
    //Testing getSelected Method but not used in app yet.
    it('getSelected() should return the selected Entity', () => {
      const result = FriendsSelectors.getSelected(state) as FriendsEntity;
      const selId = getFriendsId(result);

      expect(selId).toBe('3');
    });
    // Testing loaded status, but not used in app yet.
    it('getFriendsLoaded() should return the current "loaded" status', () => {
      const result = FriendsSelectors.getFriendsLoaded(state);

      expect(result).toBe(true);
    });
    // Testing error response, but not used in app yet.
    it('getFriendsError() should return the current "error" state', () => {
      const result = FriendsSelectors.getFriendsError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});

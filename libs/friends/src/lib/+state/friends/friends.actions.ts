import { createAction, props } from '@ngrx/store';
import { FriendsEntity } from './friends.models';

export const init = createAction('[Friends Page] Init');

export const loadFriendsSuccess = createAction(
  '[Friends/API] Load Friends Success',
  props<{ friends: FriendsEntity[] }>()
);
// Delete Friend action so we can dispatch this action i.e. this.store.dispatch(addFriend({friend}))
export const addFriend = createAction(
  '[Friends/API] Add Friend',
  props<{ friend: FriendsEntity }>()
);
// Delete Friend action so we can dispatch this action i.e. this.store.dispatch(deleteFriend({id:'5'}))
export const deleteFriend = createAction(
  '[User/API] Delete Friend',
  props<{ id: string }>()
);

export const loadFriendsFailure = createAction(
  '[Friends/API] Load Friends Failure',
  props<{ error: any }>()
);

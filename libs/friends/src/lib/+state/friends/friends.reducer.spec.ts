import { Action } from '@ngrx/store';

import * as FriendsActions from './friends.actions';
import { FriendsEntity } from './friends.models';
import { State, initialState, reducer } from './friends.reducer';

describe('Friends Reducer', () => {
  const createFriendsEntity = (
    id: string,
    name = '',
    age = 5,
    weight = 46,
    numFriends = 10
  ): FriendsEntity => ({
    id,
    name: name || `name-${id}`,
    age: age,
    weight: weight,
    numFriends: numFriends,
  });

  describe('valid Friends actions', () => {
    it('loadFriendsSuccess should return the list of known Friends', () => {
      const friends = [
        createFriendsEntity('1', 'Brian Jest', 10, 70, 20),
        createFriendsEntity('2', 'Suzzy Jest', 15, 90, 30),
      ];
      const action = FriendsActions.loadFriendsSuccess({ friends });

      const result: State = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
    //AddFriend recieves a friend {} and should return a result of one.
    it('AddFriend should add one friend to state.', () => {
      const friend: FriendsEntity = {
        id: '1',
        name: 'Brian Jest',
        age: 10,
        weight: 70,
        numFriends: 20,
      };

      const action = FriendsActions.addFriend({ friend: friend });

      const result: State = reducer(initialState, action);

      expect(result.ids.length).toBe(1);
    });
    //Need to populate the state with one, then test delete method and expect a result of zero.
    it('Delete friend should remove one friend.', () => {
      const friend: FriendsEntity = {
        id: '1',
        name: 'Brian Jest',
        age: 10,
        weight: 70,
        numFriends: 20,
      };
      // Loading State with friend we can then delete.
      const loadAction = FriendsActions.addFriend({ friend: friend });
      reducer(initialState, loadAction);
      // send {id} to deleteFriend Method to delete a Friend.
      const action = FriendsActions.deleteFriend({ id: '1' });
      const result: State = reducer(initialState, action);

      expect(result.ids.length).toBe(0);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as Action;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});

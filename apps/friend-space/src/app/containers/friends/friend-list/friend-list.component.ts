import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  deleteFriend,
  FriendsEntity,
  getAllFriends,
} from '@sw-app-test/friends';
import { Observable } from 'rxjs';

@Component({
  selector: 'sw-app-test-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.scss'],
})
export class FriendListComponent implements OnInit {
  friends$!: Observable<FriendsEntity[]>;

  @Input() showUiComponent!: string;

  onDeletedFriend(id: string) {
    this.store.dispatch(deleteFriend({ id: id }));
  }

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.friends$ = this.store.select(getAllFriends);
  }
}

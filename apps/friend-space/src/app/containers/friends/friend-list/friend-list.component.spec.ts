import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListComponent } from '@sw-app-test/ui-shared';
import { Store, StoreModule } from '@ngrx/store';
import * as fromRoot from '@sw-app-test/friends';

import { FriendListComponent } from './friend-list.component';
import { deleteFriend } from '@sw-app-test/friends';

describe('FriendListComponent', () => {
  let component: FriendListComponent;
  let fixture: ComponentFixture<FriendListComponent>;
  let store: Store<fromRoot.State>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [FriendListComponent, ListComponent],
      imports: [StoreModule.forRoot(fromRoot.reducer)],
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch the deleteFriend action when deletedFriend is called', () => {
    const removedFriendId = '1';
    const action = deleteFriend({ id: removedFriendId });
    const spy = jest.spyOn(store, 'dispatch');

    fixture.detectChanges();

    component.onDeletedFriend(removedFriendId);
    expect(spy).toHaveBeenCalledWith(action);
  });
});

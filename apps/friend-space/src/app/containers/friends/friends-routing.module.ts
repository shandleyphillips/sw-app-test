import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";

const friendRoutes: Routes = [
  { path: "home", component: HomeComponent },
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forChild(friendRoutes)],
  exports: [RouterModule],
})
export class FriendsRoutingModule {}

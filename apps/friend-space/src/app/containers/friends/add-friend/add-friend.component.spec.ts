import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { FriendFormComponent } from '@sw-app-test/ui-shared';
import * as fromRoot from '@sw-app-test/friends';

import { AddFriendComponent } from './add-friend.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { addFriend } from '@sw-app-test/friends';

describe('AddFriendComponent', () => {
  let component: AddFriendComponent;
  let fixture: ComponentFixture<AddFriendComponent>;
  let store: Store<fromRoot.State>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [AddFriendComponent, FriendFormComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot(fromRoot.reducer),
      ],
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFriendComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch the addFriend action when onAddFriend is called', () => {
    const friend = {
      id: '1',
      name: 'Brian Jest',
      age: 10,
      weight: 40,
      numFriends: 60,
    };
    const action = addFriend({ friend: friend });
    const spy = jest.spyOn(store, 'dispatch');

    fixture.detectChanges();

    component.onAddFriend(friend);
    expect(spy).toHaveBeenCalledWith(action);
  });
});

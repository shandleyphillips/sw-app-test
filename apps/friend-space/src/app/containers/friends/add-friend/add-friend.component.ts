import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { FriendsEntity, addFriend } from '@sw-app-test/friends';

@Component({
  selector: 'sw-app-test-add-friend',
  templateUrl: './add-friend.component.html',
  styleUrls: ['./add-friend.component.scss'],
})
export class AddFriendComponent {
  friends$!: Observable<FriendsEntity[]>;

  constructor(private store: Store) {}

  onAddFriend(newFriend: FriendsEntity) {
    this.store.dispatch(addFriend({ friend: newFriend }));
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FriendsRoutingModule } from './friends-routing.module';

import { HomeComponent } from './home/home.component';
import { FriendListComponent } from './friend-list/friend-list.component';
import { AddFriendComponent } from './add-friend/add-friend.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { UiSharedModule } from '@sw-app-test/ui-shared';

@NgModule({
  declarations: [HomeComponent, FriendListComponent, AddFriendComponent],
  imports: [
    CommonModule,
    FriendsRoutingModule,
    UiSharedModule,
    MatGridListModule,
    MatCardModule,
    MatDividerModule,
  ],
})
export class FriendsModule {}

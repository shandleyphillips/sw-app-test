import { Component } from '@angular/core';

@Component({
  selector: 'sw-app-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'friend-space';
}

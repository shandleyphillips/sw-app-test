module.exports = {
  projects: [
    '<rootDir>/apps/friend-space',
    '<rootDir>/libs/friends',
    '<rootDir>/libs/ui-shared',
  ],
  transform: {
    '^.+\\.(ts|js|html)$': 'jest-preset-angular',
  },
};
